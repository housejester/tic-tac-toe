# Tic-Tac-Toe - Intro
I’m new to Rust. I wanted to try out some simple code to get a better feel for it, so I thought writing a little tic-tac-toe game would be good for that. 

Initial work. I  dove in and wrote some initial code, and learned some, but it felt too scattered. I wanted to start over and do a better job of writing up what I was doing and learning. So here we are.

### Starting Out
Rust has a built in package manager and build tool called **cargo**. For Java folks, this is Maven. To start off a new binary project (ie one that will be run as a program from the command line, vs just included as a library), pop open a terminal, navigate to where you put code, and run:
`cargo init —bin tic-tac-toe`
This creates a new tic-tac-toe directory, and the typical rust program directory structure under it:
```
$ find tic-tac-toe/
tic-tac-toe
tic-tac-toe/Cargo.toml
tic-tac-toe/.gitignore
tic-tac-toe/.git/*   
tic-tac-toe/src
tic-tac-toe/src/main.rs
```

We can see it sets up the project with git for version control right off the bat (files under .git/ were removed from the output here). A quick `git status`  will show it has no commits yet. Also included is a default **.gitignore** file:
```
$ cat .gitignore
/target
**/*.rs.bk
```
Looks like cargo uses **/target** for a build output dir like maven does, and a bit strange, but I guess rust folks tend to copy rs files as .bk to make big changes?

The Cargo.toml is the project description file, including things like name, version, and dependencies:
```
[package]
name = "tic-tac-toe"
version = "0.1.0"
authors = ["James Estes <james@d8a.io>"]

[dependencies]
```
It picks up the **authors** from your git configs.  We don’t have any dependencies yet. 

Finally, there is the `src/`directory with just a `main.rs` file:
```
fn main() {
    println!("Hello, world!");
}
```
Yar.

### Building
With cargo, you can build and run with: `cargo run`:
```
$ cargo run
   Compiling tic-tac-toe v0.1.0 (file:///Users/me/dev/tic-tac-toe)
    Finished dev [unoptimized + debuginfo] target(s) in 0.47 secs
     Running `target/debug/tic-tac-toe`
Hello, world!
```

Note it reports to be running a binary under `target/debug`, which is the default build mode (debug) output location. You can also just build with `cargo build`:
```
$ cargo build
   Compiling tic-tac-toe v0.1.0 (file:///Users/me/dev/tic-tac-toe)
    Finished dev [unoptimized + debuginfo] target(s) in 0.47 secs
```

It doesn’t report the output binary location…but not hard to find it if you know to look under `target/`.  Once built, we can just run the binary:

```
$ ./target/debug/tic-tac-toe
Hello, world!
```

Ok, so what about non-`debug` builds? Pass `—release` flag to the build command:
```
$ cargo build --release
   Compiling tic-tac-toe-2 v0.1.0 (file:///Users/me/tic-tac-toe)
    Finished release [optimized] target(s) in 0.30 secs
```
And the binary lands under `target/release`:

```
$ ./target/release/tic-tac-toe
Hello, world!
```

#rust/tic-tac-toe