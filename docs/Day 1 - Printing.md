# Day 1 - Printing
Let’s build a simple, command-line tic-tac-toe game. We’ll start simple. The game will start by printing an empty board and asking for a move. The expected input will be a number 1-9, representing what “cell” you want to play the next move into. Numbering starts at the top left corner and moves left-to-right, and then down the rows:
```
1|2|3
4|5|6
7|8|9
```

Once the move is entered, the game will process it and render the updated board, asking for the next move. Lets update `main.rs` to just render the board now.

```
fn main() {
  println!("_|_|_");
  println!("_|_|_");
  println!("_|_|_");
  println!("X to play: ");
}
```
```
_|_|_
_|_|_
_|_|_
X to play:
```

Simple enough. `println!` was in the initial `main.rs` that cargo generated for us. It feels a bit odd that something like `println!` is a macro, but we’ll roll with that. 

Rust strings can also be multi-line, which could reduce the noise a bit in the initial code. We’ll eventually have a function that takes a board structure and prints it, so we really don’t need to use a multiline string, but let’s take a look at it anyway.
```
fn main() {
  println!("
    _|_|_
    _|_|_
    _|_|_
    X to play: "
  );
}
```
Output:
```
    _|_|_
    _|_|_
    _|_|_
    X to play:
```

Rust also has “[raw string literals](https://doc.rust-lang.org/reference/tokens.html#raw-string-literals)”, which are nice if your string has double quotes or escapes you don’t want rust to process. That would look more like this:
```
fn main() {
  println!(r#"
    _|_|_
    _|_|_
    _|_|_
    X to play: "#
  );
}
```
Where the initial “ is preceded by an `r` and 0 or more `#`s, so `r#”` in our case starts the raw string literal, and `”#` terminates it (the number of `#` must match, you really only need to use > 0 if you have quotes in your data.

So, back to our output. We have those spaces in the output (since we also have them in the input source file, spaced for code readability. We can remove them to render better:
```
fn main() {
  println!("
_|_|_
_|_|_
_|_|_
X to play: "
	);
}
```

```
_|_|_
_|_|_
_|_|_
X to play:
```

Now the output looks good, but code doesn’t.  This is a common problem with multiline strings. Googling around a bit, there is an [SO post](https://stackoverflow.com/questions/29483365/what-is-the-syntax-for-a-multiline-string-literal) mentioning a rust macro `indoc!` that intelligently strips out the spaces. It creates a new “left border” at the left edge of the first non-whitespace character in the string. Lets look at pulling in a crate and try it out!

```
fn main() {
  println!(indoc!("
    _|_|_
    _|_|_
    _|_|_
    X to play: "
  ));
}
```

Compile failed…looking a little closer at the SO post, `indoc!` is an external macro in a GitHub [repo](https://github.com/dtolnay/indoc).  The readme shows how to use it in a project. 

First, we want to add the import to Cargo.toml:
```
[dependencies]
indoc = "0.2"
```
Got this from the indoc repo readme. Presumably, the crate is “published” somewhere cargo can find it. 

Then add the import to `main.rs`, and use the macro:
```
#[macro_use]
extern crate indoc;

fn main() {
  println!(indoc!("
    _|_|_
    _|_|_
    _|_|_
    X to play: "
  ));
}
```

This is like an import , pulling in an external “crate”, and then an annotation above it, indicating we’ll be using the macros defined by the imported crate. 

Now to run it!
`cargo run` starts to build…we see it pull in and compile the external crate, then it hangs for seemingly forever compiling our own `main.rs` file. Seems like nesting the macros isn’t working. Not sure if that is just a Rustism or a bug. But splitting them up fixes it:

```
#[macro_use]
extern crate indoc;

fn main() {
  let board = indoc!("
    _|_|_
    _|_|_
    _|_|_
    X to play: "
  );
  println!("{}", board)
}
```
Turns out println macro doesn’t directly support c-style printf syntax, which is a bummer. It has the placeholder support though. The compiler does a nice job informing me that the `std::fmt` is where to look for formatting details. 
```
   = help: `%s` should be written as `{}`
   = note: printf formatting not supported; see the documentation for `std::fmt`
```
Which, googling, leads me here: [std::fmt - Rust](https://doc.rust-lang.org/1.24.0/std/fmt/), and shows a bunch of formatting features. So, not printf, but seems robust. 
As an aside, the standard docs are also part of your local rust toolchain install (~_.rustup_toolchains_stable-x86_64-apple-darwin_share_doc_rust_html_index.html) but more on that later. 

Output:
```
_|_|_
_|_|_
_|_|_
X to play:
```
Nice. We _really_ don’t need it, but we’ll keep it for now.

We learned:
- [ ] how to use the `println!` macro
- [ ] rust strings are multi-line
- [ ] a bit about raw string literals
- [ ] how to use external libraries (crates in rust-speak). 
- [ ] a bit about reading/finding rust docs 
- [ ] rust doesn’t do printf syntax, but has it’s own
- [ ] macros maybe don’t nest well (or the indoc macro has a bug)

#rust/tic-tac-toe