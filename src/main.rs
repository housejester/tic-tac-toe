use tic_tac_toe::Game;
use std::io;
use std::io::Write;

fn main() {
  let mut game = Game::new();

  while !game.get_status().is_over(){
    game_loop(&mut game);
  }
  render_board(&game.get_board());
  println!("{}", game.get_status().message());
}

pub fn game_loop(game: &mut Game) {
  render_board(&game.get_board());
  print!("{}: ", game.get_status().message());
  std::io::stdout().flush().unwrap();

  let mut input = String::new();
  match io::stdin().read_line(&mut input) {
    Ok(_) => {
      let res = game.play(input.trim());
      match res {
        Ok(_st) => {},
        Err(err) => println!("{}", err),
      }
    }
    Err(error) => {
        println!("error: {}", error);
    }
  }
}

fn render_board(board: &[char; 9]) {
  println!("{}|{}|{}", board[0], board[1], board[2]);
  println!("{}|{}|{}", board[3], board[4], board[5]);
  println!("{}|{}|{}", board[6], board[7], board[8]);
}

mod tic_tac_toe {
  #[derive(PartialEq, Debug, Copy, Clone)]
  #[repr(usize)]
  pub enum Status {
    ExTurn = 0,
    ExWon = 1,
    OhTurn = 2,
    OhWon = 3,
    CatWon = 4,
    Quit = 5,
  }

  struct StatusDetail {
    name: &'static str,
    message: &'static str,
    game_over: bool,
  }

  const STATUSES: [StatusDetail; 6] = [
    StatusDetail{name: "ExTurn", message: "X to play", game_over: false},
    StatusDetail{name: "ExWon", message: "X Wins!", game_over: true},
    StatusDetail{name: "OhTurn", message: "O to play", game_over: false},
    StatusDetail{name: "OhWon", message: "O wins!", game_over: true},
    StatusDetail{name: "CatWon", message: "Cat got it!", game_over: true},
    StatusDetail{name: "Quit", message: "Player quit", game_over: true},
  ];

  impl Status {
      pub fn is_over(&self) -> bool {
        return STATUSES[*self as usize].game_over;
      }
      pub fn message(&self) -> &str {
        return STATUSES[*self as usize].message;
      }
  }

  pub struct Game {
    board: [char; 9],
    quit: bool,
  }

  const WINS: [[usize; 3]; 8] = [[0,1,2],[3,4,5],[6,7,8],[0,3,6],[1,4,7],[2,5,8],[0,4,8],[2,4,6]];

  impl Game {
    pub fn new() -> Game {
      return Game{board: ['_'; 9], quit: false}
    }
    pub fn get_status(&self) -> Status {
      if self.quit {
        return Status::Quit;
      }
      for (_, win_locs) in WINS.iter().enumerate() {
        let (i1, i2, i3) = (win_locs[0], win_locs[1], win_locs[2]);
        let (v1, v2, v3) = (self.board[i1], self.board[i2], self.board[i3]);

        if v1 == v2 && v2 == v3 && v1 != '_' {
          if v1 == 'X' {
            return Status::ExWon;
          }
          if v1 == 'O' {
            return Status::OhWon;
          }
        }
      }
      let e_cnt = self.board.iter().filter(|s| **s == '_').count() as i32;
      if e_cnt == 0 {
        return Status::CatWon;
      }
      let x_cnt = self.board.iter().filter(|s| **s == 'X').count() as i32;
      let o_cnt = self.board.iter().filter(|s| **s == 'O').count() as i32;
      if x_cnt == o_cnt {
        return Status::ExTurn;
      }
      // todo could do validation here too. eg o_cnt > x_cnt, means o cheated. diff > 1, x cheated. 
      // x and o both have wins, someone flubbed.
      return Status::OhTurn;
    }

    pub fn get_board(&self) -> [char; 9] {
      return self.board
    }
    pub fn play(&mut self, value: &str) -> Result<Status, &str> {
      match value {
        "exit" | "quit" | "q" => {
          self.quit=true;
          return Ok(self.get_status());
        },
        _ => {
          let maybe_cell = i32::from_str_radix(value, 10);
          match maybe_cell {
            Ok(cell) => {
              return self.play_at(cell);
            },
            _ => {
              return Err("Specify a cell location 1-9");
            }
          }
        }
      }
    }
    fn play_at(&mut self, loc_in: i32) -> Result<Status, &str> {
      if loc_in < 1 || loc_in > 9 {
        return Err("Specify cell number 1-9");
      }
      let loc: usize = (loc_in - 1) as usize;
      if self.board[loc as usize] != '_' {
        return Err("Space not empty")
      }
      match self.get_status() {
        Status::ExTurn => {
          self.board[loc as usize] = 'X';
        },
        Status::OhTurn => {
          self.board[loc as usize] = 'O';
        },
        _ => return Err("Invalid state..."),
      }
      return Ok(self.get_status());
    }
  }
}

#[cfg(test)]
mod tests {
  use tic_tac_toe::Game;
  use tic_tac_toe::Status;

  #[test]
  fn test_initial_state() {
    let game = Game::new();
    assert_eq!(game.get_status(), Status::ExTurn);
  }

  #[test]
  fn test_play_game() {
    let mut game = Game::new();
    {
      let res = game.play("1");
      assert_eq!(res, Ok(Status::OhTurn));
    }
    assert_eq!(game.get_board(), board("X________"));
    assert_eq!(game.get_status(), Status::OhTurn);

    {
      let res = game.play("2");
      assert_eq!(res, Ok(Status::ExTurn));
    }
    assert_eq!(game.get_board(), board("XO_______"));
    assert_eq!(game.get_status(), Status::ExTurn);

    {
      let res = game.play("3");
      assert_eq!(res, Ok(Status::OhTurn));
    }
    assert_eq!(game.get_board(), board("XOX______"));
    assert_eq!(game.get_status(), Status::OhTurn);

    {
      let res = game.play("4");
      assert_eq!(res, Ok(Status::ExTurn));
    }
    assert_eq!(game.get_board(), board("XOXO_____"));
    assert_eq!(game.get_status(), Status::ExTurn);

    {
      let res = game.play("5");
      assert_eq!(res, Ok(Status::OhTurn));
    }
    assert_eq!(game.get_board(), board("XOXOX____"));
    assert_eq!(game.get_status(), Status::OhTurn);

    {
      let res = game.play("6");
      assert_eq!(res, Ok(Status::ExTurn));
    }
    assert_eq!(game.get_board(), board("XOXOXO___"));
    assert_eq!(game.get_status(), Status::ExTurn);

    {
      let res = game.play("7");
      assert_eq!(res, Ok(Status::ExWon));
    }
    assert_eq!(game.get_board(), board("XOXOXOX__"));
    assert_eq!(game.get_status(), Status::ExWon);

    // test we don't change board if we play again, after game over
    {
      let res = game.play("8");
      assert_eq!(res, Err("Invalid state..."));
    }
    assert_eq!(game.get_board(), board("XOXOXOX__"));
    assert_eq!(game.get_status(), Status::ExWon);
    
  }

  fn board(text: &str) -> [char; 9] {
    let mut iter = text.chars();
    [
      iter.next().unwrap(),iter.next().unwrap(),iter.next().unwrap(),
      iter.next().unwrap(),iter.next().unwrap(),iter.next().unwrap(),
      iter.next().unwrap(),iter.next().unwrap(),iter.next().unwrap(),
    ]
  }
}